# Phaethon

Hardware design files for Phaethon ZigBee and BLE boards

Phaethon (Φαέθων) was the son of the Oceanid Clymene and the sun god Helios in Greek mythology.

The name "Phaethon", which means "Shining One", was given to one of the horses of Eos (the Dawn),
 the Sun, the constellation Auriga, and the planet Jupiter.

Phaethon is a series of sensors for Temperature, Humidity, Barometric Pressure, Motion, etc.
The sensors are based on Nordic's nRF52 microcontroller series.
The nRF52 series microcontrollers have an ARM Cortex M4 core and integrate an RF tranciever operating at 2.4GHz.

The sensors advertise their measurements over Bluetooth Low Energy (BLE) or over ZigBee Home Automation (ZB HA).

# Phaethon Temperature and Humidity (Phaethon-TnH)

![board](./images/phaethon-tnh-kicad.jpg)

Phaethon TnH features a Sensirion SHT30-DIS temperature and humidity sensor IC
 and a BMP280 (or compatible) barometric pressure sensor.
